/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : CardManager
 * Scrit Description This script manages the physics of the card according to three states :  
 * Card is on the hand, Card is released,  Card is on the ground.
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    private Rigidbody objectRigidBody;
    private Renderer objectRenderer;

    private void Awake()
    {
        objectRigidBody = GetComponent<Rigidbody>();
        objectRenderer = GetComponent<Renderer>();
        objectRenderer.material.color = Color.green;
    }

    /*When object is on the hand, physics for the object are enabled.
    This allows object to fall like a normal object if user releases the object. */
    public void ObjectOnHand()
    {
        objectRenderer.material.color = Color.red;
        objectRigidBody.isKinematic = false; 
        objectRigidBody.useGravity = true;
        CancelInvoke("DisablePhysics");
        /*After 60 seconds on the hand, object is considered to be in the door slot 
         and physics are disabled for performances optimization */
        Invoke("DisablePhysics", 60f);
    }


    /*Object physics are disabled after 4 seconds if objects is released in the air
    Object physics are disabled instantly if object is released in the door slot */
    public void ObjectReleased()
    {
        objectRenderer.material.color = new Color(1F, 0.5F, 0.2F); // Orange Color
        objectRigidBody.isKinematic = false;
        objectRigidBody.useGravity = true;
        Invoke("DisablePhysics", 4.0f);   
    }

    public void DisablePhysics()
    {
        objectRenderer.material.color = Color.green;
        objectRigidBody.isKinematic = true;
        objectRigidBody.useGravity = false;
    }
}
