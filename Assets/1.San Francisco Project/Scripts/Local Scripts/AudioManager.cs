/* Author name : Diego M. (Taking as a reference Introduction to AUDIO in Unity by Brakeys)
 * Project Name : San Francisco House 
 * Script Name : AudioManager
 * Scrit Description : This script manages the audio required during the gameplay. 
 */
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] soundsArray;
    private void Awake()
    {
        foreach (Sound item in soundsArray)
        {
            item.source = gameObject.AddComponent<AudioSource>();
            item.source.clip = item.clip;
            item.source.volume = item.volume;
            item.source.loop = item.loop;
        }
    }


    private void Start()
    {
        Play("AmbientMusic");
    }

    //Play function is accessible from other scripts
    public  void Play (string soundName)
    {
        Sound soundFound = Array.Find(soundsArray, sound => sound.name == soundName);
        if(soundFound == null)
        {
            Debug.LogError("Audio Source not found ");
            return;
        }
        soundFound.source.Play();
    }
}
