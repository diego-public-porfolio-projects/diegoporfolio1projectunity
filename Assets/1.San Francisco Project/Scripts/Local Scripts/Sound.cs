/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : Sound
 * Scrit Description : This sound class represents the template for sound object used in Audio Manager script 
 */

using UnityEngine.Audio;
using UnityEngine;


[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;
   
    [Range(0f,1f)]
    public float volume;
    [Range(.1f, 3f)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource source;
}
