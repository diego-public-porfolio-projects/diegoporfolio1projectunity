/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : PlayerInput
 * Scrit Description : This script uses the new  input system and calls an specific event according to specific input acions.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] InputActionAsset actionAsset;
    InputAction menuAction;

    public delegate void ClickAction();
    public static event ClickAction secondaryButtonPerformed;

    void Start()
    {
        var rhActionMap = actionAsset.FindActionMap("XRI RightHand");
        menuAction = rhActionMap.FindAction("MenuOpenClose");
        menuAction.Enable();
        menuAction.performed += MenuButtonPerformed;
    }

    //There is an event called secondaryButtonPerformed every single timpe the menu button is pressed
    private void MenuButtonPerformed( InputAction.CallbackContext context)
    {
        secondaryButtonPerformed();
    }
}
