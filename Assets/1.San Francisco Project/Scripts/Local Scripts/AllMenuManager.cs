/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : AllMenuManager
 * Scrit Description : This script takes the specific input from the right controller and handles the activation and deactivation of 
 * all the menus (introduction menu, main menu and secondary menu)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pixelplacement;
using Pixelplacement.TweenSystem;

public class AllMenuManager : MonoBehaviour
{
    [Header("Camera transform")]
    [SerializeField] Transform cameraTransform;

    [Header("All menus transforms")]
    [SerializeField] Transform mainMenuCanvasTransform;
    [SerializeField] Transform secondarycanvasTransform;
    [SerializeField] Transform introductionMenuCanvasTransform;
    
    private bool MainMenuActivated = false;
    
    void Start()
    {
        //Subscribing MainMenuActivation method to the event defined  in PlayerInput 
        PlayerInput.secondaryButtonPerformed += MainMenuActivation;

        //Initializing All Menu Canvas
        mainMenuCanvasTransform.gameObject.SetActive(false);
        secondarycanvasTransform.gameObject.SetActive(false);
        introductionMenuCanvasTransform.gameObject.SetActive(false);
        DisableChildPanelsSecondaryMenu();
        //Introduction Menu is visible at the beginning of the game experience
        ActiveIntroductionMenu();
    }


     void Update()
    {
        if (MainMenuActivated)
        {
            mainMenuCanvasTransform.LookAt(cameraTransform.position);
            mainMenuCanvasTransform.Rotate(0, 180f, 0);

            secondarycanvasTransform.LookAt(cameraTransform.position);
            secondarycanvasTransform.Rotate(0, 180f, 0);
        }
    }



    //Main Menu canvas is active  when menu button is pressed (secondary button of RH Controller)
    public void MainMenuActivation()
    {
        MainMenuActivated = !MainMenuActivated;
        DisableChildPanelsSecondaryMenu();
       
        if (MainMenuActivated)
        {
            //Initialize main menu position according to the camera
            Vector3 ForwardDirection = new Vector3(cameraTransform.forward.x, -0.5f, cameraTransform.forward.z);
            mainMenuCanvasTransform.position = cameraTransform.position + ForwardDirection  * 0.55f;
            mainMenuCanvasTransform.gameObject.SetActive(true);
            //SoundManager.PlaySound("MenuOpen", mainMenuCanvasTransform.position);

            //Initialize secondary menu position according to the main menu position
            secondarycanvasTransform.position = cameraTransform.position + ForwardDirection * 1f + (Vector3.up * 0.75f);         
            secondarycanvasTransform.gameObject.SetActive(true);
            FindObjectOfType<AudioManager>().Play("OpenMenu");
        }
        else
        {
            //Deactivate main menu and secondary menu
            mainMenuCanvasTransform.gameObject.SetActive(false);
            secondarycanvasTransform.gameObject.SetActive(false);
            FindObjectOfType<AudioManager>().Play("CloseMenu");
        }
        return;
    }

    //Disable all the child panels of the secondary menu  when main menu is closed
    private void DisableChildPanelsSecondaryMenu()
    {
        for (int i = 0; i < 5; i++)
        {
            secondarycanvasTransform.GetChild(i).gameObject.SetActive(false);
        }
    }


    #region Intoducion Menu Code

    //Initialize and activate Introduction Menu
    private void ActiveIntroductionMenu()
    {
        introductionMenuCanvasTransform.gameObject.SetActive(true);
        //Introduction Menu is not available after 40 seconds
        Invoke("DeActiveIntroductionMenu", 40.0f);
    }

    //Deactive Introduction Menu
    private void DeActiveIntroductionMenu()
    {
        introductionMenuCanvasTransform.gameObject.SetActive(false);
    }

    #endregion

}



