/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : DoorSlotController
 * Scrit Description This script manages the opennning/closing  of the door if the card is inside/outside the door slot.
 * functions are called externally by the XR Socket Interactor component attached to the Door Slot Controller Script.
 * Door animation is done using Pixelplacement package
 * Pixel Package available in : https://assetstore.unity.com/packages/tools/utilities/surge-107312
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class DoorSlotController  : MonoBehaviour
{
    [SerializeField] Transform _doorTransform;

    float animationDuration = 2f;
    float startDelayAnimation = 1f;

    public void CardSocketInside()
    {
        Tween.Rotation(_doorTransform, new Vector3(0, 90, 0), animationDuration, startDelayAnimation);
        FindObjectOfType<AudioManager>().Play("Door");
        //AudioManager.Play("Door");
        //SoundManager.PlaySound("Door", _doorTransform.position);
    }

    public void CardSocketOutside()
    {
        Tween.Rotation(_doorTransform, new Vector3(0, 0, 0), animationDuration, startDelayAnimation);
        FindObjectOfType<AudioManager>().Play("Door");
        //AudioManager.Play("Door");
    }
}

