/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : InteractionController
 * Scrit Description This script takes the specific input from the right controller and changes the interaction mode accordingly. (New Input System)
 * There are two interaction modes :
 * Direct Interacor = Objects are grabbed manually using the RH controller.
 * Ray Interactor = Objects are grabbed using the RH controller ray.
 */


using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class InteractionController : MonoBehaviour
{
    [SerializeField] InputActionReference switchInteractorReference;
    [SerializeField] InputActionReference menuOpenClose;

    public UnityEvent directModeEvent;
    public UnityEvent rayModeEvent;

    private bool swichInteractor = false;
    private bool menuOpen = false;

    void Start()
    {
        //Attach the input action reference to the function. (New Input System)
        switchInteractorReference.action.performed += SwitchInteractorPerformed;
        //Openning the menu automatically activates ray interaction mode
        menuOpenClose.action.performed += MenuOpenClosePerformed;
        //Direct mode is used by default
        directModeEvent.Invoke();
    }


    // Change the state of switchInteractor variable
    // switchInteractor= true (ray mode activated),  switchInteractor = false (ray mode deactivated)
    private void SwitchInteractorPerformed(InputAction.CallbackContext context)
    {
        swichInteractor = !swichInteractor;
        RayModeEvaluation();
    }


    // Change the state of menuOpen variable
    // menuOpen= true (menu open),  menuOpen = false (menu closed)
    private void MenuOpenClosePerformed(InputAction.CallbackContext context)
    {
        
        menuOpen = !menuOpen;
        RayModeEvaluation();
    }

    //Ray mode is enabled if menu is opened or if switch for ray interaction is pressed
    private void RayModeEvaluation()
    {
        if ((swichInteractor) || (menuOpen))
        { 
            rayModeEvent.Invoke();
        }
        else
        {
            directModeEvent.Invoke();
        }
    }
}
