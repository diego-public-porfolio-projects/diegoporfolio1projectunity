/* Author name : Diego M. 
 * Project Name : San Francisco House 
 * Script Name : TeleportController
 * Scrit Description : This script manages the change between de LH base controller and LH teleport controller. 
 */
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class TeleportController : MonoBehaviour
{
    public GameObject baseControllerGameObject;
    public GameObject teleportationGameObject;

    public InputActionReference teleportActivationReference;

    [Header("Events")]
    public UnityEvent onTeleportActivate;
    public UnityEvent onTeleportCancel;

    void Start()
    {
        onTeleportCancel.Invoke();
        teleportActivationReference.action.performed += TeleportModeActivate;
        teleportActivationReference.action.canceled += TeleportModeCancel;
    }

    private void TeleportModeCancel(InputAction.CallbackContext obj)
    {
        Invoke("DeactivateTeleporter", 0.1f);
    }

    private void TeleportModeActivate(InputAction.CallbackContext obj)
    {
        onTeleportActivate.Invoke();
    }

    void DeactivateTeleporter()
    {
        onTeleportCancel.Invoke();
    }

}
